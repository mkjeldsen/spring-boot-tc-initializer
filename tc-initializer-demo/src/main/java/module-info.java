import org.jspecify.annotations.NullMarked;

@NullMarked
open module io.gitlab.mkjeldsen.tcinitializerdemo {
  requires spring.context;
  requires spring.boot.autoconfigure;
  requires spring.boot;
  requires org.jspecify;
}
