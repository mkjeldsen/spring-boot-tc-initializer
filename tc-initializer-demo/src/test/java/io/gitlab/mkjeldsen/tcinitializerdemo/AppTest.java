package io.gitlab.mkjeldsen.tcinitializerdemo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.regex.Pattern;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@EnableTestcontainers
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
final class AppTest {

  @Autowired Environment env;

  @Test
  void context_loads() {
    assertThat(env).isNotNull();
  }

  @Test
  void sets_spring_rabbitmq_port() {
    final var key = "spring.rabbitmq.port";
    assertThat(env.getProperty(key)).describedAs(key).isNotEmpty().matches(Pattern.compile("\\d+"));
  }

  @Test
  void sets_spring_rabbitmq_host() {
    final var key = "spring.rabbitmq.host";
    assertThat(env.getProperty(key)).describedAs(key).isNotEmpty();
  }

  @Test
  void sets_spring_data_redis_port() {
    final var key = "spring.data.redis.port";
    assertThat(env.getProperty(key)).describedAs(key).isNotEmpty().matches(Pattern.compile("\\d+"));
  }

  @Test
  void sets_spring_data_redis_host() {
    final var key = "spring.data.redis.host";
    assertThat(env.getProperty(key)).describedAs(key).isNotEmpty();
  }

  @Test
  void sets_spring_datasource_password() {
    final var key = "spring.datasource.password";
    assertThat(env.getProperty(key)).describedAs(key).isNotEmpty();
  }

  @Test
  void sets_spring_datasource_username() {
    final var key = "spring.datasource.username";
    assertThat(env.getProperty(key)).describedAs(key).isNotEmpty();
  }

  @Test
  void sets_spring_datasource_url() {
    final var key = "spring.datasource.url";
    assertThat(env.getProperty(key)).describedAs(key).isNotEmpty();
  }
}
