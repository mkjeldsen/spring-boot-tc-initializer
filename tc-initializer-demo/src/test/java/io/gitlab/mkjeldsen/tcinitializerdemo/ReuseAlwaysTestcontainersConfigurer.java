package io.gitlab.mkjeldsen.tcinitializerdemo;

import io.gitlab.mkjeldsen.tcinitializer.spi.TestcontainersConfigurer;
import org.jspecify.annotations.NullMarked;
import org.testcontainers.utility.TestcontainersConfiguration;

@NullMarked
public final class ReuseAlwaysTestcontainersConfigurer implements TestcontainersConfigurer {

  @Override
  public void configure(TestcontainersConfiguration configuration) {
    configuration.updateUserConfig("testcontainers.reuse.enable", "" + true);
  }
}
