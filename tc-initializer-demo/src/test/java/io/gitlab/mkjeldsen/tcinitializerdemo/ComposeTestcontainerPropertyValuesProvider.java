package io.gitlab.mkjeldsen.tcinitializerdemo;

import io.gitlab.mkjeldsen.tcinitializer.spi.TestcontainerPropertyValuesProvider;
import java.io.File;
import org.jspecify.annotations.NullMarked;
import org.springframework.boot.test.util.TestPropertyValues;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.lifecycle.Startable;

@NullMarked
public final class ComposeTestcontainerPropertyValuesProvider
    implements TestcontainerPropertyValuesProvider {

  private final ComposeContainer compose;

  public ComposeTestcontainerPropertyValuesProvider() {
    compose =
        new ComposeContainer(new File("compose.yaml"))
            .withLocalCompose(false)
            .withExposedService("rabbitmq", 5672)
            .withExposedService("redis", 6379);
  }

  @Override
  public Startable getContainer() {
    return compose;
  }

  @Override
  public TestPropertyValues getPropertyValues() {
    var rabbitmqHost = compose.getServiceHost("rabbitmq", 5672);
    var rabbitmqPort = compose.getServicePort("rabbitmq", 5672);
    var redisHost = compose.getServiceHost("redis", 6379);
    var redisPort = compose.getServicePort("redis", 6379);
    return TestPropertyValues.of(
        "spring.rabbitmq.host=" + rabbitmqHost, "spring.rabbitmq.port=" + rabbitmqPort,
        "spring.data.redis.host=" + redisHost, "spring.data.redis.port=" + redisPort);
  }
}
