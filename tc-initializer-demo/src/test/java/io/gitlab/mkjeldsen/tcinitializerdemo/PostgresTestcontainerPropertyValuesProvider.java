package io.gitlab.mkjeldsen.tcinitializerdemo;

import io.gitlab.mkjeldsen.tcinitializer.spi.TestcontainerPropertyValuesProvider;
import org.jspecify.annotations.NullMarked;
import org.springframework.boot.test.util.TestPropertyValues;
import org.testcontainers.containers.JdbcDatabaseContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.lifecycle.Startable;
import org.testcontainers.utility.DockerImageName;

@SuppressWarnings("resource")
@NullMarked
public final class PostgresTestcontainerPropertyValuesProvider
    implements TestcontainerPropertyValuesProvider {

  private final JdbcDatabaseContainer<?> postgres;

  public PostgresTestcontainerPropertyValuesProvider() {
    var image = DockerImageName.parse("postgres:16");
    postgres = new PostgreSQLContainer<>(image).withReuse(true);
  }

  @Override
  public Startable getContainer() {
    return postgres;
  }

  @Override
  public TestPropertyValues getPropertyValues() {
    var jdbcUrl = postgres.getJdbcUrl();
    var username = postgres.getUsername();
    var password = postgres.getPassword();
    return TestPropertyValues.of(
        "spring.datasource.url=" + jdbcUrl,
        "spring.datasource.username=" + username,
        "spring.datasource.password=" + password);
  }
}
