import io.gitlab.mkjeldsen.tcinitializer.spi.TestcontainerPropertyValuesProvider;
import io.gitlab.mkjeldsen.tcinitializer.spi.TestcontainersConfigurer;
import org.jspecify.annotations.NullMarked;

@NullMarked
module io.gitlab.mkjeldsen.tcinitializer {
  uses TestcontainerPropertyValuesProvider;
  uses TestcontainersConfigurer;

  exports io.gitlab.mkjeldsen.tcinitializer;
  exports io.gitlab.mkjeldsen.tcinitializer.spi;

  requires org.jspecify;
  requires spring.context;
  requires testcontainers;
  requires spring.boot.test;
}
