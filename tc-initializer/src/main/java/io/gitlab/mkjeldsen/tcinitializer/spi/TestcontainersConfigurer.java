package io.gitlab.mkjeldsen.tcinitializer.spi;

import org.testcontainers.utility.TestcontainersConfiguration;

public interface TestcontainersConfigurer {

  void configure(TestcontainersConfiguration configuration);
}
