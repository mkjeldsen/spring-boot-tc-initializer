package io.gitlab.mkjeldsen.tcinitializer.spi;

import org.springframework.boot.test.util.TestPropertyValues;
import org.testcontainers.lifecycle.Startable;

public interface TestcontainerPropertyValuesProvider {

  Startable getContainer();

  TestPropertyValues getPropertyValues();
}
